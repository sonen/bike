<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'John Doe',
            'email' => 'johndoe@cash.com',
            'password' => Hash::make('123456')
        ]);
        DB::table('users')->insert([
            'name' => 'Jane Doe',
            'email' => 'janedoe@sinatra.com',
            'password' => Hash::make('123456')
        ]);
    }
}
