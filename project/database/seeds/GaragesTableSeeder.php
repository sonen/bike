<?php

use App\Garage;
use Illuminate\Database\Seeder;

class GaragesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('garages')->delete();
        $json = File::get("database/database-sample/garages.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            Garage::create(array(
                'id' => $obj->id,
                'name' => $obj->name,
                'customer_level' => $obj->customer_level
            ));
        }
    }
}
